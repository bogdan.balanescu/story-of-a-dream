import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class World2 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class World2 extends World
{
    public int texttime = 400;
    public int leveltime = 1000;
    /**
     * Constructor for objects of class World2.
     * 
     */
    public World2()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600 , 400, 1); 
        Text2 text2 = new Text2();
        addObject(text2, 298, 133);
        populate();
    }

    public void act()
    {
        Instructions();
        LevelUp();
        cheaty();
    }

    public void cheaty()
    {
        if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("1"))
        {
            Greenfoot.setWorld(new Hi());
        }
    }
    
    public void LevelUp()
    {
        if(leveltime>0)
        {
            leveltime--;
            if(leveltime == 0)
            {
                addObject(new NextLevelText(), getWidth()/2, getHeight()/2);
            }
        }

        if(leveltime == 0 && Greenfoot.isKeyDown("space"))
        {
            Greenfoot.setWorld(new World3());
        }

    }

    public void Instructions()
    {
        if( texttime > 0)
        {
            texttime--;
            if(texttime == 0)
            {
                populate2();
            }
        }
    }
    
    public void populate()
    {
        Character2 character2 = new Character2();
        addObject(character2, 302, 338);
    }

    public void populate2()
    {        
        int direction = Greenfoot.getRandomNumber(360);
        int x = Greenfoot.getRandomNumber(100) + 32;
        int y = Greenfoot.getRandomNumber(100) + 109;
        Ball2 ball2 = new Ball2(direction);
        addObject(ball2, x, y);
        direction = Greenfoot.getRandomNumber(360);
        x = Greenfoot.getRandomNumber(100) + 32;
        y = Greenfoot.getRandomNumber(100) + 109;
        Ball2 ball3 = new Ball2(direction);
        addObject(ball3, x, y);
        direction = Greenfoot.getRandomNumber(360);
        x = Greenfoot.getRandomNumber(450) + 32;
        y = Greenfoot.getRandomNumber(100) + 109;
        Ball2 ball4 = new Ball2(direction);
        addObject(ball4, x, y);
    }
}
