import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class World5 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class World5 extends World
{
    private int texttime = 500;
    private int x;
    private int y;
    private int secondwave = 140;
    private int wave1 = 1;
    private int wave2 = 1;
    private int text = 1;
    private int next = 0;
    /**
     * Constructor for objects of class World5.
     * 
     */
    public World5()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        Text5 text5 = new Text5();
        addObject(text5, getWidth()/2, getHeight()/2);
        populate();
    }

    public void act()
    {
        if(texttime > 0)
        {
            texttime--;
        }
        else
        {
            if(wave1 == 1)
            {
                populate2();
                wave1 = 0;
            }
            else if(secondwave > 0)
            {
                secondwave--;
            }
            if(wave2 == 1 && secondwave == 0)
            {
                populate3();
                wave2 = 0;
            }
            if(numberOfObjects() == 1 && wave2 == 0)
            {
                if(text == 1)
                {
                    addObject(new NextLevelText(), getWidth()/2, getHeight()/2);
                    text = 0;
                    next = 1;
                }
            }
            if(next == 1 && Greenfoot.isKeyDown("space"))
            {
                Greenfoot.setWorld(new World6());
            }
        }
        cheaty();
    }

    public void cheaty()
    {
        if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("1"))
        {
            Greenfoot.setWorld(new Hi());
        }
    }
    
    public void populate()
    {
        Spaceship5 spaceship5 = new Spaceship5();
        addObject(spaceship5, 57, getHeight()/2);
    }

    public void populate2()
    {
        x = Greenfoot.getRandomNumber(200) + 400;
        y = Greenfoot.getRandomNumber(400);
        Alien5 alien5 = new Alien5();
        addObject(alien5, x, y);
        x = Greenfoot.getRandomNumber(200) + 400;
        y = Greenfoot.getRandomNumber(400);
        Alien5 alien6 = new Alien5();
        addObject(alien6, x, y);
        x = Greenfoot.getRandomNumber(200) + 400;
        y = Greenfoot.getRandomNumber(400);
        Alien5 alien7 = new Alien5();
        addObject(alien7, x, y);
    }

    public void populate3()
    {
        Alien5 alien8 = new Alien5();
        addObject(alien8, x, y);
        x = Greenfoot.getRandomNumber(200) + 400;
        y = Greenfoot.getRandomNumber(400);
        Alien5 alien9 = new Alien5();
        addObject(alien9, x, y);
        x = Greenfoot.getRandomNumber(200) + 400;
        y = Greenfoot.getRandomNumber(400);
        Alien5 alien10 = new Alien5();
        addObject(alien10, x, y);
    }

    public void explosion(int x, int y)
    {
        Explosion boom = new Explosion();
        addObject(boom, x, y);
    }
}
