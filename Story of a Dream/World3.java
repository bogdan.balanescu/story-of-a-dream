import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class World3 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class World3 extends World
{
    private int texttime = 500;
    private int pop = 1;
    /**
     * Constructor for objects of class World3.
     * 
     */
    public World3()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        Text3 text3 = new Text3();
        addObject(text3, getWidth()/2, getHeight()/2);
        populate();
    }
    
    public void act()
    {
        if(texttime > 0)
        {
            texttime--;
        }
        else if(pop == 1)
        {
            populate2();
            pop = 0;
        }
        cheaty();
    }
    
    public void cheaty()
    {
        if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("1"))
        {
            Greenfoot.setWorld(new Hi());
        }
    }
    
    public void populate()
    {
        Character2 character2 = new Character2();
        addObject(character2, 302, 338);
    }
    
    public void populate2()
    {
        int direction = Greenfoot.getRandomNumber(360);
        int x = Greenfoot.getRandomNumber(600);
        int y = Greenfoot.getRandomNumber(400);
        Ball3 ball3 = new Ball3(direction);
        addObject(ball3, x, y);
    }
}
