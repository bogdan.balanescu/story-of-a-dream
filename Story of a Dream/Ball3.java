import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class Ball3 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Ball3 extends Instinct
{
    private int deltaX;
    private int deltaY;
    GreenfootSound circuit = new GreenfootSound("Short Circuit-SoundBible.com-1450168875.wav");
    GreenfootSound bounce1 = new GreenfootSound("androidballbounce1.wav");
    GreenfootSound bounce2 = new GreenfootSound("androidballbounce2.wav");
    
    
    /**
     * Create a ball.
     */
    public Ball3(int direction)
    {
        deltaX = Greenfoot.getRandomNumber(3) + 4;
        deltaY = Greenfoot.getRandomNumber(3) + 4;
    }
    
    /**
     * Act - do whatever the Ball wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move();
        if( canSee(Character2.class))
        {
            getWorld().addObject(new NextLevelText(), getWorld().getWidth()/2, getWorld().getHeight()/2);
            circuit.play();
            getWorld().removeObject(this);
        }
    } 
    
     /**
     * Move the ball. Then check whether we've hit a wall.
     */
    public void move()
    {
        checkWalls();
        setLocation (getX() + deltaX, getY() + deltaY);
        int warp = Greenfoot.getRandomNumber(400);
        if(warp <= 1)
        {
            int x = Greenfoot.getRandomNumber(600);
            int y = Greenfoot.getRandomNumber(400);
            setLocation (x, y);
        }
    }
    
    /**
     * Check whether we've hit one of the walls. Reverse direction if necessary.
     */
    private void checkWalls()
    {
        if (getX() == 0 || getX() == getWorld().getWidth()-1) {
            deltaX = -deltaX;
            bounce1.setVolume(90);
            bounce1.play();
        }
        if (getY() == 0 || getY() == getWorld().getHeight()-1) {
            deltaY = -deltaY;
            bounce2.setVolume(90);
            bounce2.play();
        }
    }
    
}
