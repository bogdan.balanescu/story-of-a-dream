import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Write a description of class Ending2 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Ending2 extends World
{
    GreenfootImage img = getBackground();
    /**
     * Constructor for objects of class Hi.
     * 
     */
    public Ending2()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1);
        FinalText2 finaltext2 = new FinalText2();
        addObject(finaltext2, 351, 351);
    }
}