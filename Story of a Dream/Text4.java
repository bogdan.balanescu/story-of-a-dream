import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class Text4 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Text4 extends Actor
{
    private int firstText = 100;
    private int secondText = 100;
    private int thirdText = 100;
    private int fourthText = 100;

    public Text4()
    {
        setImage(new GreenfootImage("These are the mountains", 30, Color.BLACK, null));
    }

    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        newText();
    }    

    /**
     * 
     */
    public void newText()
    {
        if( firstText > 0)
        {
            firstText--;
            if(firstText == 0)
            {
                setImage(new GreenfootImage("that you wanted to visit!", 30, Color.BLACK, null));
            }
        }
        else if(secondText > 0)
        {
            secondText--;
            if(secondText == 0)
            {
                setImage(new GreenfootImage("Escape the monsters and collect the gold!", 30, Color.BLACK, null));
            }
        }
        else if(thirdText > 0)
        {
            thirdText--;
            if(thirdText == 0)
            {
                setImage(new GreenfootImage("Controls: w,a,s,d.", 30, Color.BLACK, null));
            }
        }
        else if(fourthText > 0)
        {
            fourthText--;
            if(fourthText == 0)
            {
                getWorld().removeObject(this);
            }
        }
    }
}
