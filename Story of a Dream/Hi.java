import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;

/**
 * Write a description of class Hi here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer
 * @version (a version number or a date)
 */
public class Hi extends World
{
    GreenfootImage img = getBackground();
    private int soundtrack = 1;
    /**
     * Constructor for objects of class Hi.
     * 
     */
    public Hi()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        changeBackgroundColor();
        drawRandomCircles(100);
        Text text = new Text();
        addObject(text, getWidth()/2, getHeight()/2);
        
    }
    
    public void act()
    {
        cheat();
    }
    
    public void cheat()
    {
        if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("2"))
        {
            Greenfoot.setWorld(new World2());
        }
        else if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("3"))
        {
            Greenfoot.setWorld(new World3());
        }
        else if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("4"))
        {
            Greenfoot.setWorld(new World4());
        }
        else if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("5"))
        {
            Greenfoot.setWorld(new World5());
        }
        else if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("6"))
        {
            Greenfoot.setWorld(new World6());
        }
        else if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("7"))
        {
            Greenfoot.setWorld(new World7());
        }
        else if(Greenfoot.isKeyDown("b") && Greenfoot.isKeyDown("a") && Greenfoot.isKeyDown("c"))
        {
            Greenfoot.setWorld(new Ending());
        }
    }
    
    public void changeBackgroundColor()
    {
        int r = Greenfoot.getRandomNumber(256);
        int g = Greenfoot.getRandomNumber(256);
        int b = Greenfoot.getRandomNumber(256);
        img.setColor(new Color (r, g, b));
        img.fill();
    }
    
    public void drawRandomCircles(int howMany)
    {
        int i = 0;
        while (i < howMany) 
        {
            drawCircle();
            i = i+1;
        }
    }
    
    /**
     * Draw a circle with random size and colour.
     */
    public void drawCircle()    
    {
        GreenfootImage image = getBackground();
        int x = Greenfoot.getRandomNumber(getWidth());
        int y = Greenfoot.getRandomNumber(getHeight());
        int r = Greenfoot.getRandomNumber(256);
        int g = Greenfoot.getRandomNumber(256);
        int b = Greenfoot.getRandomNumber(256);
        Color color = new Color (r, g, b, 20);
        int size = Greenfoot.getRandomNumber(100) + 50;
        image.setColor(color);
        image.fillOval(x, y, size, size);
    }
    
}
