import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Character2 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Character2 extends Instinct
{
    public int switchWorld4 = 0;
    private int animationCounter = 0;
    private int frame = 1;
    private static int animationStep = 10;
    private GreenfootImage stang = new GreenfootImage("character-left.png");
    private GreenfootImage drept = new GreenfootImage("character-right.png");
    private GreenfootImage stand = new GreenfootImage("character-standing.png");
    
    GreenfootSound foot1 =new GreenfootSound("Footstep.wav");
    GreenfootSound foot2 =new GreenfootSound("Footstep2.wav");
    GreenfootSound coin = new GreenfootSound("Super Mario Bros Coin.wav");
    GreenfootSound punch = new GreenfootSound("Strong_Punch-Mike_Koenig-574430706.wav");
    
    private int switchWorld5 = 0;
    private int ok5 = 1;
    private static int goldnumber = 5;

    public Character2()
    {
        setImage(stand);
    }

    /**
     * Act - do whatever the Character2 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkMove();
        animationCounter++;
        checkWorld4();
        checkWorld5();
        checkIfOverLevel2();
        checkIfOverLevel4();
    }    

    public void checkMove()
    {
        if(Greenfoot.isKeyDown("w"))
        {
            moveUp();
        }
        if(Greenfoot.isKeyDown("a"))
        {
            turn(-4);
        }
        if(Greenfoot.isKeyDown("d"))
        {
            turn(4);
        }
        if(Greenfoot.isKeyDown("s"))
        {
            moveDown();
        }
    }

    public void moveUp()
    {
        move(2);
        if(animationCounter % animationStep == 0)
        {
            animateUp();
        }
    }

    public void moveDown()
    {
        move(-2);
        if(animationCounter % animationStep == 0)
        {
            animateDown();
        }
    }

    public void animateUp()
    {
        if(frame == 1)
        {
            setImage(stang);
            foot1.setVolume(90);
            foot1.play();
        }
        else if(frame == 2)
        {
            setImage(stand);
        }
        else if(frame == 3)
        {
            setImage(drept);
            foot2.setVolume(90);
            foot2.play();
        }
        else if(frame ==4)
        {
            setImage(stand);
            frame=1;
            return;
        }
        frame++;
    }

    public void animateDown()
    {
        if(frame == 4)
        {
            setImage(stang);
            foot1.setVolume(90);
            foot1.play();
        }
        else if(frame == 3)
        {
            setImage(stand);
        }
        else if(frame == 2)
        {
            setImage(drept);
            foot2.setVolume(90);
            foot2.play();
        }
        else if(frame == 1)
        {
            setImage(stand);
            frame=4;
            return;
        }
        frame--;
    }

    public void checkWorld4()
    {        
        if( canSee(Ball3.class))
        {
            switchWorld4 = 1;
        }
        if(switchWorld4 == 1 && Greenfoot.isKeyDown("space"))
        {
            Greenfoot.setWorld(new World4());
        }
    }

    public void checkWorld5()
    {
        Actor actor = getOneObjectAtOffset(0, 0, Gold4.class);
        if(actor!=null)
        {
            coin.setVolume(90);
            coin.play();
            getWorld().removeObject(actor);
            switchWorld5++;
        }
        if(switchWorld5 == goldnumber)
        {
            if(ok5 == 1)
            {
                getWorld().addObject(new NextLevelText(), getWorld().getWidth()/2, getWorld().getHeight()/2);
                ok5 = 0;
            }
            if(Greenfoot.isKeyDown("space"))
            {
                Greenfoot.setWorld(new World5());
            }    
        }    
    }

    public void checkIfOverLevel2()
    {
        if( canSee(Ball2.class))
        {
            getWorld().addObject(new GameOverText(), getWorld().getWidth()/2, getWorld().getHeight()/2);
            punch.setVolume(95);
            punch.play();
            Greenfoot.setWorld(new Ending2());
        }
    }

    public void checkIfOverLevel4()
    {
        if( canSee(Alien4.class))
        {
            getWorld().addObject(new GameOverText(), getWorld().getWidth()/2, getWorld().getHeight()/2);
            punch.setVolume(95);
            punch.play();
            Greenfoot.setWorld(new Ending2());
        }
    }
}
