import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bird here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Bird extends Instinct
{
    private int x = Greenfoot.getRandomNumber(100);
    private static int movestep = 100;
    private int step = movestep;
    private int flipswitch = 0;
    private int flipright = 0;
    private int flipleft = 0;
    private int z;
    
    GreenfootSound flap1 = new GreenfootSound("flap1.wav");
    GreenfootSound flap2 = new GreenfootSound("flap2.wav");
    GreenfootSound flap3 = new GreenfootSound("flap3.wav");
    private int soundnumber;
    
    public Bird()
    {
        soundnumber = Greenfoot.getRandomNumber(3) + 1;
        if(soundnumber == 1)
        {
            flap1.play();
        }
        else if(soundnumber == 2)
        {
            flap2.play();
        }
        else if(soundnumber == 3)
        {
            flap3.play();
        }
        if(x > 49)
        {
            flipswitch = 1;
            getImage().mirrorHorizontally();
            flipleft = 1;
        }
        else 
        {
            flipright = 1;
        }
    }

    /**
     * Act - do whatever the Bird wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        randomDirection();
        randomMove();
        removeBird();
    }    

    public void randomDirection()
    {
        if(step > 0)
        {
            step--;
        }
        else
        {
            x = Greenfoot.getRandomNumber(100);
            step = 100;
            if(flipswitch != 0)
            {
                if(flipleft == 1 && x <= 49)
                {
                    getImage().mirrorHorizontally();
                    flipleft = 0;
                    flipright = 1;
                }
                else if(flipright == 1 && x > 49)
                {
                    getImage().mirrorHorizontally();
                    flipleft = 1;
                    flipright = 0;
                }
            }
            else if(flipswitch == 0 && x > 49)
            {
                getImage().mirrorHorizontally();
                flipswitch = 1;
                flipright = 1;
            }
        }
    }

    public void randomMove()
    {
        if(x <= 49)
        {
            z = Greenfoot.getRandomNumber(3);
            setLocation(getX() - z, getY() - 1);
        }
        else
        {
            z = Greenfoot.getRandomNumber(3);
            setLocation(getX() + z, getY() - 1);
        }
    }

    public void removeBird()
    {
        if(atWorldEdge())
        {
            getWorld().removeObject(this);
        }
    }
}
