import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class FinalText here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class FinalText extends Actor
{
    private int firstText = 100;
    private int secondText = 100;
    private int thirdText = 100;
    private int fourthText = 100;
    private int fifthText = 100;
    private int sixthText = 100;
    private int seventhText = 100;
    private int continueDelay = 10;
    public FinalText()
    {
        setImage(new GreenfootImage("Congratulations!", 50, Color.BLACK, null));
    }

    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        newText();
        /*if( continueDelay > 0 && seventhText == 0)
        {
            continueDelay--;
        }
        if (continueDelay == 0 && Greenfoot.isKeyDown("space"))
        {
            Greenfoot.setWorld(new Hi());
        }
        if (continueDelay == 0 && Greenfoot.isKeyDown("f"))
        {
            Greenfoot.stop();
        }*/
    }    

    /**
     * 
     */
    public void newText()
    {
        if( firstText > 0)
        {
            firstText--;
            if(firstText == 0)
            {
                setImage(new GreenfootImage("You have finished the game!", 30, Color.BLACK, null));
                Ending ending = (Ending) getWorld();
                ending.changeBackgroundColor();
                ending.drawRandomCircles(100);
            }
        }
        else if(secondText > 0)
        {
            secondText--;
            if(secondText == 0)
            {
                setImage(new GreenfootImage("It was all a tiring dream..", 30, Color.BLACK, null));
                Ending ending = (Ending) getWorld();
                ending.changeBackgroundColor();
                ending.drawRandomCircles(100);
            }
        }
        else if(thirdText > 0)
        {
            thirdText--;
            if(thirdText == 0)
            {
                setImage(new GreenfootImage("but you made it.", 30, Color.BLACK, null));
                Ending ending = (Ending) getWorld();
                ending.changeBackgroundColor();
                ending.drawRandomCircles(100);
            }
        }
        else if(fourthText > 0)
        {
            fourthText--;
            if(fourthText == 0)
            {
                setImage(new GreenfootImage("You are now rested.", 30, Color.BLACK, null));
                Ending ending = (Ending) getWorld();
                ending.changeBackgroundColor();
                ending.drawRandomCircles(100);
            }
        }
        else if(fifthText > 0)
        {
            fifthText--;
            if(fifthText == 0)
            {
                setImage(new GreenfootImage("Have a nice day!", 30, Color.BLACK, null));
                Ending ending = (Ending) getWorld();
                ending.changeBackgroundColor();
                ending.drawRandomCircles(100);
            }
        }
        else if(sixthText > 0)
        {
            sixthText--;
            if(sixthText == 0)
            {
                setImage(new GreenfootImage("You can either start again (space)", 30, Color.BLACK, null));
                Ending ending = (Ending) getWorld();
                ending.changeBackgroundColor();
                ending.drawRandomCircles(100);
            }
        }
        else if(seventhText > 0)
        {
            seventhText--;
            if(seventhText == 0)
            {
                setImage(new GreenfootImage("or stop this instant (f)", 30, Color.BLACK, null));
                Ending ending = (Ending) getWorld();
                ending.changeBackgroundColor();
                ending.drawRandomCircles(100);
            }
        }
    }
}
