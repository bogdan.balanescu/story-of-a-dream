import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Explosion here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Explosion extends Actor
{
    private static int explosiontimeset = 30;
    private int explosiontime1 = explosiontimeset;
    private int explosiontime2 = explosiontimeset;
    GreenfootImage image = new GreenfootImage("explosion-2.png");
    /**
     * Act - do whatever the Explosion wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(explosiontime1 > 0)
        {
            explosiontime1--;
        }
        else
        {
            setImage(image);
        }
        if(explosiontime2 > 0 && explosiontime1 == 0)
        {
            explosiontime2--;
        }
        if(explosiontime2 == 0 && explosiontime1 == 0)
        {
            getWorld().removeObject(this);
        }
    }    
}
