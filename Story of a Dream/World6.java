import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class World6 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class World6 extends World
{
    private int texttime = 500;
    private static int cloudsnumber = 10;
    private static int birdnumber = 30;
    private int birdsleft = birdnumber;
    private int timeleft = 25;
    private int number;
    private int next;
    private int x;
    private int y;
    /**
     * Constructor for objects of class World6.
     * 
     */
    public World6()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        Text6 text6 = new Text6();
        addObject(text6, getWidth()/2, getHeight()/2);
        populate();
        clouds();
    }

    public void act()
    {
        if(texttime > 0)
        {
            texttime--;
        }
        else
        {
            generateBirds();
            if(birdsleft == 0)
            {
                number = numberOfObjects();
                if(number == 1 + cloudsnumber)
                {
                    addObject(new NextLevelText(), getWidth()/2, getHeight()/2);
                    next = 1;
                }
            }
            if(next == 1 && Greenfoot.isKeyDown("space"))
            {
                Greenfoot.setWorld(new World7());
            }
        }
        cheaty();
    }

    public void cheaty()
    {
        if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("1"))
        {
            Greenfoot.setWorld(new Hi());
        }
    }
    
    public void populate()
    {
        FallingMan fallingman = new FallingMan();
        addObject(fallingman, getWidth()/2, 43);
    }

    public void clouds()
    {
        for(int i = 1; i<= cloudsnumber; i++)
        {
            x = Greenfoot.getRandomNumber(600);
            y = Greenfoot.getRandomNumber(400);
            addObject(new Clouds(), x, y);
        }
    }
    
    public void generateBirds()
    {
        if(birdsleft > 0 && timeleft == 0)
        {
            birdsleft--;
            int y = Greenfoot.getRandomNumber(150) + 250;
            addObject(new Bird(), getWidth()/2, y);
            timeleft = Greenfoot.getRandomNumber(125);
        }
        else if(birdsleft > 0 && timeleft > 0)
        {
            timeleft--;
        }
    }

}
