import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class FallingMan here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class FallingMan extends Instinct
{
    private int animationCounter = 0;
    private int frame = 1;
    private static int animationStep = 7;
    private GreenfootImage stang = new GreenfootImage("man3.png");
    private GreenfootImage drept = new GreenfootImage("man2.png");
    private GreenfootImage stand = new GreenfootImage("man1.png");

    GreenfootSound punch = new GreenfootSound("Super Punch MMA-SoundBible.com-1869306362.wav");
    GreenfootSound wind = new GreenfootSound("wind-howl-01.wav");
    
    /**
     * Act - do whatever the FallingMan wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkMove();
        checkCollision();
        if(!wind.isPlaying())
        {
            wind.setVolume(50);
            wind.play();
        }
    }    
    
    public void checkMove()
    {
        if(Greenfoot.isKeyDown("a"))
        {
            moveLeft();
        }
        if(Greenfoot.isKeyDown("d"))
        {
            moveRight();
        }
        if(!Greenfoot.isKeyDown("d") && !Greenfoot.isKeyDown("a"))
        {
            setImage(stand);
        }
    }

    public void moveRight()
    {
        move(2);
        if(animationCounter % animationStep == 0)
        {
            setImage(drept);
        }
    }
    
    public void moveLeft()
    {
        move(-2);
        if(animationCounter % animationStep == 0)
        {
            setImage(stang);
        }
    }
    
    public void checkCollision()
    {
        if(canSee(Bird.class))
        {
            getWorld().addObject(new GameOverText(), getWorld().getWidth()/2, getWorld().getHeight()/2);
            punch.setVolume(90);
            punch.play();
            Greenfoot.setWorld(new Ending2());
        }
    }
    
}
