import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
import java.awt.Font;
/**
 * Write a description of class Text here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Text extends Actor
{
    private int firstText = 100;
    private int secondText = 100;
    private int thirdText = 100;
    private int fourthText = 100;
    private int fifthText = 100;
    private int sixthText = 100;
    private int seventhText = 100;
    private int continueDelay = 10;

    public Text()
    {
        setImage(new GreenfootImage("Welcome!", 50, Color.BLACK, null));
    }

    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        newText();
        //continueDelay = 0; /**WATCH OUT!!!!*/
        if( continueDelay > 0 && seventhText == 0)
        {
            continueDelay--;
        }
        if (continueDelay == 0 && Greenfoot.isKeyDown("space"))
        {
            Greenfoot.setWorld(new World2());
        }
    }    

    /**
     * 
     */
    public void newText()
    {
        if( firstText > 0)
        {
            firstText--;
            if(firstText == 0)
            {
                setImage(new GreenfootImage("In this game you will find yourself", 30, Color.BLACK, null));
                Hi hi = (Hi) getWorld();
                hi.changeBackgroundColor();
                hi.drawRandomCircles(100);
            }
        }
        else if(secondText > 0)
        {
            secondText--;
            if(secondText == 0)
            {
                setImage(new GreenfootImage("going through a series of challenges!", 30, Color.BLACK, null));
                Hi hi = (Hi) getWorld();
                hi.changeBackgroundColor();
                hi.drawRandomCircles(100);
            }
        }
        else if(thirdText > 0)
        {
            thirdText--;
            if(thirdText == 0)
            {
                setImage(new GreenfootImage("You will be playing as Hero,", 30, Color.BLACK, null));
                Hi hi = (Hi) getWorld();
                hi.changeBackgroundColor();
                hi.drawRandomCircles(100);
            }
        }
        else if(fourthText > 0)
        {
            fourthText--;
            if(fourthText == 0)
            {
                setImage(new GreenfootImage("a little man that is about to", 30, Color.BLACK, null));
                Hi hi = (Hi) getWorld();
                hi.changeBackgroundColor();
                hi.drawRandomCircles(100);
            }
        }
        else if(fifthText > 0)
        {
            fifthText--;
            if(fifthText == 0)
            {
                setImage(new GreenfootImage("have a nightmare!", 30, Color.BLACK, null));
                Hi hi = (Hi) getWorld();
                hi.changeBackgroundColor();
                hi.drawRandomCircles(100);
            }
        }
        else if(sixthText > 0)
        {
            sixthText--;
            if(sixthText == 0)
            {
                setImage(new GreenfootImage("Be prepared for a final full of surprizes!", 30, Color.BLACK, null));
                Hi hi = (Hi) getWorld();
                hi.changeBackgroundColor();
                hi.drawRandomCircles(100);
            }
        }
        else if(seventhText > 0)
        {
            seventhText--;
            if(seventhText == 0)
            {
                setImage(new GreenfootImage("Press 'space' to continue...", 30, Color.BLACK, null));
                Hi hi = (Hi) getWorld();
                hi.changeBackgroundColor();
                hi.drawRandomCircles(100);
            }
        }
    }
}
