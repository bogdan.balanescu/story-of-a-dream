import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Spaceship5 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Spaceship5 extends Instinct
{
    private int shotTimer = 0;
    GreenfootImage image = getImage();
    GreenfootImage image2 = new GreenfootImage("spaceship-fire.png");
    GreenfootSound engine = new GreenfootSound("Rocket Thrusters-SoundBible.com-1432176431.wav");
    GreenfootSound laser1 = new GreenfootSound("Laser Blasts-SoundBible.com-108608437.wav");
    GreenfootSound laser2 = new GreenfootSound("Laser Blasts-SoundBible.com-108608437.wav");

    /**
     * Act - do whatever the Spaceship5 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkMove();
        shootBullet();
    }    

    public void checkMove()
    {
        if(Greenfoot.isKeyDown("w"))
        {
            move(3);
            setImage(image2);
            engine.play();
        }
        if(Greenfoot.isKeyDown("a"))
        {
            turn(-4);
        }
        if(Greenfoot.isKeyDown("d"))
        {
            turn(4);
        }
        if(Greenfoot.isKeyDown("s"))
        {
            move(-3);
            setImage(image);
            engine.play();
        }
        if(!Greenfoot.isKeyDown("s") && !Greenfoot.isKeyDown("w"))
        {
            setImage(image);
            engine.stop();
        }
    }

    public void shootBullet()
    {
        if(shotTimer > 0)
        {
            shotTimer--;
        }
        else if(Greenfoot.isKeyDown("space"))
        {
            getWorld().addObject(new SpaceBullet(getRotation()), getX(), getY());
            if(laser1.isPlaying())
            {
                laser2.setVolume(85);
                laser2.play();
            }
            else
            {
                laser1.setVolume(85);
                laser1.play();
            }
            shotTimer = 50;
        }
    }
}
