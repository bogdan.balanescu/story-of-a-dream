import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class AlienBullet here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class AlienBullet extends Instinct
{
    GreenfootSound explosion = new GreenfootSound("Bomb_Exploding-Sound_Explorer-68256487.wav");
    
    public AlienBullet(int direction)
    {
        setRotation(direction);
    }

    /**
     * Act - do whatever the AlienBullet wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move(7);
        setGameOver();
        removeAtWorldEdge();
    }    

    public void removeAtWorldEdge()
    {
        if(atWorldEdge())
        {
            getWorld().removeObject(this);
        }
    }

    public void setGameOver()
    {

        if(canSee(Spaceship5.class))
        {
            Actor actor = getOneObjectAtOffset(0, 0, Spaceship5.class);
            if(actor!=null)
            {
                explosion.setVolume(90);
                explosion.play();
                getWorld().addObject(new GameOverText(), getWorld().getWidth()/2, getWorld().getHeight()/2);
                Greenfoot.setWorld(new Ending2());
            }
        }
    }
}
