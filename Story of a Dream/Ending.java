import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class Final here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer
 * @version (a version number or a date)
 */
public class Ending extends World
{
    GreenfootImage img = getBackground();
    GreenfootSound song = new GreenfootSound("Best wake up sound.mp3");
    private int timer = 800;
    /**
     * Constructor for objects of class Hi.
     * 
     */
    public Ending()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        changeBackgroundColor();
        drawRandomCircles(100);
        EndingClock endingclock = new EndingClock();
        addObject(endingclock, getWidth()/2, getHeight()/2);
        FinalText finaltext = new FinalText();
        addObject(finaltext, getWidth()/2, 27);

    }

    public void act()
    {
        if(!song.isPlaying())
        {
            song.play();
        }
        if (Greenfoot.isKeyDown("space") && Greenfoot.isKeyDown("l"))
        {
            song.stop();
            Greenfoot.setWorld(new Hi());
        }
        if (Greenfoot.isKeyDown("f") && Greenfoot.isKeyDown("l"))
        {
            song.stop();
            Greenfoot.stop();
        }
        timeEnd();
    }

    public void timeEnd()
    {
        if(timer > 0)
        {
            timer--;
        }
        else 
        {
            if (Greenfoot.isKeyDown("space"))
            {
                song.stop();
                Greenfoot.setWorld(new Hi());
            }
            if (Greenfoot.isKeyDown("f"))
            {
                song.stop();
                Greenfoot.stop();
            }
        }
    }

    public void changeBackgroundColor()
    {
        int r = Greenfoot.getRandomNumber(150);
        int g = Greenfoot.getRandomNumber(150);
        int b = Greenfoot.getRandomNumber(256);
        img.setColor(new Color (r, g, b));
        img.fill();
    }

    public void drawRandomCircles(int howMany)
    {
        int i = 0;
        while (i < howMany) 
        {
            drawCircle();
            i = i+1;
        }
    }

    /**
     * Draw a circle with random size and colour.
     */
    public void drawCircle()    
    {
        GreenfootImage image = getBackground();
        int x = Greenfoot.getRandomNumber(getWidth());
        int y = Greenfoot.getRandomNumber(getHeight());
        int r = Greenfoot.getRandomNumber(256);
        int g = Greenfoot.getRandomNumber(256);
        int b = Greenfoot.getRandomNumber(256);
        Color color = new Color (r, g, b, 20);
        int size = Greenfoot.getRandomNumber(100) + 50;
        image.setColor(color);
        image.fillOval(x, y, size, size);
    }
}
