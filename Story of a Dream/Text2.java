import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
import java.awt.Font;
/**
 * Write a description of class Text here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Text2 extends Actor
{
    private int firstText = 100;
    private int secondText = 100;
    private int thirdText = 100;
    private int continueDelay = 10;

    public Text2()
    {
        setImage(new GreenfootImage("You had a bad day at work", 30, Color.BLACK, null));
    }

    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        newText();
    }    

    /**
     * 
     */
    public void newText()
    {
        if( firstText > 0)
        {
            firstText--;
            if(firstText == 0)
            {
                setImage(new GreenfootImage("You work with balls ;))", 30, Color.BLACK, null));
            }
        }
        else if(secondText > 0)
        {
            secondText--;
            if(secondText == 0)
            {
                setImage(new GreenfootImage("Survive by moving with W, A, S, and D", 30, Color.BLACK, null));
            }
        }
        else if(thirdText > 0)
        {
            thirdText--;
            if(thirdText == 0)
            {
                getWorld().removeObject(this);
            }
        }
        
    }
}
