import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
/**
 * Write a description of class Alien5 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Alien5 extends Instinct
{
    GreenfootImage image = new GreenfootImage("alienship-fire.png");
    GreenfootSound laser = new GreenfootSound("Laser_Cannon-Mike_Koenig-797224747 (mp3cut.net).wav");
    
    public Alien5()
    {
        setImage(image);
        turn(Greenfoot.getRandomNumber(360));
    }
    
    public void act()
    {
        move(4);
        randomTurn();
        turnAtEdge();
        randomBullet();
    }
    
    /**
     * With a 10% probability, turn a bit right or left.
     */
    public void randomTurn()
    {
        if ( Greenfoot.getRandomNumber(100) < 10 )
        {
            turn( Greenfoot.getRandomNumber(40)-20 );
        }        
    }
    
    /**
     * If we reach the edge of the world, turn a little bit.
     */
    public void turnAtEdge()
    {
        if (atWorldEdge())
        {
            turn(7);
        }
    }
    
    public void randomBullet()
    {
        int bullet = Greenfoot.getRandomNumber(200);
        if(bullet <= 1)
        {
            laser.setVolume(85);
            laser.play();
            getWorld().addObject(new AlienBullet(getRotation()), getX(), getY());
        }
    }
}
