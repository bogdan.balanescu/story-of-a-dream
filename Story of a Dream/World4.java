import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class World4 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class World4 extends World
{
    private int texttime = 500;
    private int pop = 1;
    /**
     * Constructor for objects of class World4.
     * 
     */
    public World4()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        Text4 text4 = new Text4();
        addObject(text4, getWidth()/2, getHeight()/2);
        populate();
    }
    
    public void act()
    {
        if(texttime > 0)
        {
            texttime--;
        }
        else if(pop == 1)
        {
            populate2();
            pop = 0;
        }
        cheaty();
    }
    
    public void cheaty()
    {
        if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("1"))
        {
            Greenfoot.setWorld(new Hi());
        }
    }
    
    public void populate()
    {
        Character2 character2 = new Character2();
        addObject(character2, 302, 338);
    }
    
    public void populate2()
    {
        int x = Greenfoot.getRandomNumber(600);
        int y = Greenfoot.getRandomNumber(400);
        Alien4 alien4 = new Alien4();
        addObject(alien4, x, y);
        x = Greenfoot.getRandomNumber(600);
        y = Greenfoot.getRandomNumber(300);
        Alien4 alien5 = new Alien4();
        addObject(alien5, x, y);
        x = Greenfoot.getRandomNumber(600);
        y = Greenfoot.getRandomNumber(300);
        Gold4 gold4 = new Gold4();
        addObject(gold4, x, y);
        x = Greenfoot.getRandomNumber(600);
        y = Greenfoot.getRandomNumber(300);
        Gold4 gold5 = new Gold4();
        addObject(gold5, x, y);
        x = Greenfoot.getRandomNumber(600);
        y = Greenfoot.getRandomNumber(300);
        Gold4 gold6 = new Gold4();
        addObject(gold6, x, y);
        x = Greenfoot.getRandomNumber(600);
        y = Greenfoot.getRandomNumber(300);
        Gold4 gold7 = new Gold4();
        addObject(gold7, x, y);
        x = Greenfoot.getRandomNumber(600);
        y = Greenfoot.getRandomNumber(300);
        Gold4 gold8 = new Gold4();
        addObject(gold8, x, y);
    }
}
