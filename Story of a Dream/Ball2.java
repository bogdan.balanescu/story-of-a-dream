import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class Ball2 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Ball2 extends Instinct
{
    private int deltaX;
    private int deltaY;
    GreenfootSound bounce = new GreenfootSound("Ball2.wav");
    
    /**
     * Create a ball.
     */
    public Ball2(int direction)
    {
        deltaX = Greenfoot.getRandomNumber(5);
        deltaY = Greenfoot.getRandomNumber(5);
    }
    
    /**
     * Act - do whatever the Ball wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move();
    } 
    
        /**
     * Move the ball. Then check whether we've hit a wall.
     */
    public void move()
    {
        checkWalls();
        setLocation (getX() + deltaX, getY() + deltaY);
        
    }
    
    /**
     * Check whether we've hit one of the walls. Reverse direction if necessary.
     */
    private void checkWalls()
    {
        if (getX() <=10 + 22 || getX() >= getWorld().getWidth()-11 -22) {
            deltaX = -deltaX;
            bounce.setVolume(90);
            bounce.play();
        }
        if (getY() <= 87 +22 || getY() >= getWorld().getHeight()-31 -22) {
            deltaY = -deltaY;
            bounce.setVolume(90);
            bounce.play();
        }
    }
    
}
