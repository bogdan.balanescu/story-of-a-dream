import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class World7 here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class World7 extends World
{
    private int texttime = 500;
    private static int totalboxes = 13;
    private int boxesleft = totalboxes;
    private static int boxinterval = 125;
    private int timeleft = boxinterval;
    /**
     * Constructor for objects of class World7.
     * 
     */
    public World7()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        Text7 text7 = new Text7();
        addObject(text7, getWidth()/2, getHeight()/2);
        populate();
    }
    
    public void act()
    {
        if(texttime > 0)
        {
            texttime--;
        }
        else
        {
            generateCrates();
        }
        cheaty();
    }
    
    public void cheaty()
    {
        if(Greenfoot.isKeyDown("l") && Greenfoot.isKeyDown("1"))
        {
            Greenfoot.setWorld(new Hi());
        }
    }
    
    public void generateCrates()
    {
        if(boxesleft > 0 && timeleft == 0)
        {
            boxesleft--;
            addObject(new Box(), 599, 209);
            timeleft = boxinterval;
        }
        else if(boxesleft > 0 && timeleft > 0)
        {
            timeleft--;
        }
        else if(boxesleft == 0)
        {
            addObject(new Clock(), 313, 140);
        }
    }
    
    public void populate()
    {
        WalkingMan walkingman = new WalkingMan();
        addObject(walkingman, 38, 209);
    }
}
