import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class FinalText2 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class FinalText2 extends Actor
{
    private int firstText = 100;
    private int secondText = 100;
    private int thirdText = 100;
    private int fourthText = 100;
    private int fifthText = 100;
    private int sixthText = 100;
    private int seventhText = 100;
    private int continueDelay = 10;
    public FinalText2()
    {
        setImage(new GreenfootImage("You have lost!", 50, Color.BLACK, null));
    }

    /**
     * Act - do whatever the Text wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        newText();
        if( continueDelay > 0 && seventhText == 0)
        {
            continueDelay--;
        }
        if (continueDelay == 0 && Greenfoot.isKeyDown("space"))
        {
            Greenfoot.setWorld(new Hi());
        }
        if (continueDelay == 0 && Greenfoot.isKeyDown("f"))
        {
            Greenfoot.stop();
        }
    }    

    /**
     * 
     */
    public void newText()
    {
        if( firstText > 0)
        {
            firstText--;
            if(firstText == 0)
            {
                setImage(new GreenfootImage("You have not made it to the end.", 30, Color.BLACK, null));
            }
        }
        else if(secondText > 0)
        {
            secondText--;
            if(secondText == 0)
            {
                setImage(new GreenfootImage("Therefore all your progress has been lost.", 30, Color.BLACK, null));
            }
        }
        else if(thirdText > 0)
        {
            thirdText--;
            if(thirdText == 0)
            {
                setImage(new GreenfootImage("You are not rested well", 30, Color.BLACK, null));
            }
        }
        else if(fourthText > 0)
        {
            fourthText--;
            if(fourthText == 0)
            {
                setImage(new GreenfootImage("and you will have a bad day.", 30, Color.BLACK, null));
            }
        }
        else if(fifthText > 0)
        {
            fifthText--;
            if(fifthText == 0)
            {
                setImage(new GreenfootImage("But there is one good news:", 30, Color.BLACK, null));
            }
        }
        else if(sixthText > 0)
        {
            sixthText--;
            if(sixthText == 0)
            {
                setImage(new GreenfootImage("You can start again (space)", 30, Color.BLACK, null));
            }
        }
        else if(seventhText > 0)
        {
            seventhText--;
            if(seventhText == 0)
            {
                setImage(new GreenfootImage("or stop this nonsense game (f).", 30, Color.BLACK, null));
            }
        }
    }
}
