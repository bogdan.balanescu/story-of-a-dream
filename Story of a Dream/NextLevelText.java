import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class NextLevelText here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class NextLevelText extends Actor
{
    /**
     * Act - do whatever the NextLevelText wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        setImage(new GreenfootImage("Press 'space' to get to the next level", 30, Color.BLACK, null));
    }    
}
