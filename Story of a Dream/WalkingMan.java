import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class WalkingMan here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class WalkingMan extends Instinct
{
    private int animationCounter = 0;
    private int frame = 1;
    private static int animationStep = 10;
    private GreenfootImage stang = new GreenfootImage("walk3.png");
    private GreenfootImage drept = new GreenfootImage("walk2.png");
    private GreenfootImage stand = new GreenfootImage("walk1.png");
    private int jump = 0;
    private int down = 0;

    GreenfootSound left = new GreenfootSound("leftfootgrass.wav");
    GreenfootSound right = new GreenfootSound("rightfootgrass.wav");
    GreenfootSound punch = new GreenfootSound("Super Punch MMA-SoundBible.com-1869306362.wav");

    /**
     * Act - do whatever the WalkingMan wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        checkMove();
        checkJump();
        doJump();
        animationCounter++;
        checkHit();
        checkClock();
    }

    public void checkClock()
    {
        if(canSee(Clock.class))
        {
            eat(Clock.class);
            Greenfoot.setWorld(new Ending());
        }
    }

    public void checkHit()
    {
        if(canSee(Box.class))
        {
            getWorld().addObject(new GameOverText(), getWorld().getWidth()/2, getWorld().getHeight()/2);
            punch.setVolume(90);
            punch.play();
            Greenfoot.setWorld(new Ending2());
        }
    }

    public void checkJump()
    {
        if(Greenfoot.isKeyDown("w") && jump == 0)
        {
            jump = 1;
        }
    }

    public void doJump()
    {
        if(jump == 1)
        {
            if(getY() > 111 && down == 0)
            {
                setLocation(getX(), getY() - 4);
                if(getY() <= 111)
                {
                    down = 1;
                }
            }
            else if(down == 1)
            {
                setLocation(getX(), getY() + 4);
                if(getY() >= 209)
                {
                    down = 0;
                    jump = 0;
                }
            }
        }
    }

    public void checkMove()
    {
        if(Greenfoot.isKeyDown("a"))
        {
            moveLeft();
        }
        if(Greenfoot.isKeyDown("d"))
        {
            moveRight();
        }
    }

    public void moveRight()
    {
        move(2);
        if(animationCounter % animationStep == 0)
        {
            animateRight();
        }
    }

    public void moveLeft()
    {
        move(-2);
        if(animationCounter % animationStep == 0)
        {
            animateLeft();
        }
    }

    public void animateLeft()
    {
        if(frame == 1)
        {
            setImage(stang);
            if(jump == 0)
            { 
                left.setVolume(90);
                left.play();
            }
        }
        else if(frame == 2)
        {
            setImage(stand);
        }
        else if(frame == 3)
        {
            setImage(drept);
            if(jump == 0)
            { 
                right.setVolume(90);
                right.play();
            }
        }
        else if(frame ==4)
        {
            setImage(stand);
            frame=1;
            return;
        }
        frame++;
    }

    public void animateRight()
    {
        if(frame == 4)
        {
            setImage(stang);
            if(jump == 0)
            { 
                left.setVolume(90);
                left.play();
            }
        }
        else if(frame == 3)
        {
            setImage(stand);
        }
        else if(frame == 2)
        {
            setImage(drept);
            if(jump == 0)
            { 
                right.setVolume(90);
                right.play();
            }
        }
        else if(frame == 1)
        {
            setImage(stand);
            frame=4;
            return;
        }
        frame--;
    }

}
