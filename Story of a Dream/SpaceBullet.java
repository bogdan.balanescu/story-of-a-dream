import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class SpaceBullet here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class SpaceBullet extends Instinct
{
    GreenfootSound explosion = new GreenfootSound("Bomb_Exploding-Sound_Explorer-68256487.wav");
    
    public SpaceBullet(int direction)
    {
        setRotation(direction);
    }
    
    /**
     * Act - do whatever the SpaceBullet wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move(7);
        removeAlien();
        removeAtWorldEdge();
    }    
    
    public void removeAtWorldEdge()
    {
        if(atWorldEdge())
        {
            getWorld().removeObject(this);
        }
    }
    
    public void removeAlien()
    {
        if(canSee(Alien5.class))
        {
            Actor actor = getOneObjectAtOffset(0, 0, Alien5.class);
            if(actor!=null)
            {
                getWorld().removeObject(actor);
                World5 myworld5 = (World5) getWorld();
                explosion.setVolume(90);
                explosion.play();
                myworld5.explosion(getX(), getY());
            }
        }
    }
}
