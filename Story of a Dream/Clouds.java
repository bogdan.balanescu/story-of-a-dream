import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Clouds here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class Clouds extends Actor
{
    GreenfootImage image = new GreenfootImage("nor2.png");
    private int x;
    private int y;
    
    public Clouds()
    {
        int z = Greenfoot.getRandomNumber(100);
        if(z<=49)
        {
            setImage(image);
        }
    }
    /**
     * Act - do whatever the Clouds wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        move();
    }    
    
    public void move()
    {
        x = Greenfoot.getRandomNumber(5) - 2;
        y = Greenfoot.getRandomNumber(5) - 2;
        setLocation(getX() + x, getY() + y);
    }
}
