import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class GameOverText here.
 * 
 * @author 1: Bogdan-Daniel Bălănescu
 * @author 2: Răzvan-Dalian Cucer 
 * @version (a version number or a date)
 */
public class GameOverText extends Actor
{
    private int firstText = 100;
    /**
     * Act - do whatever the GameOverText wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public GameOverText() 
    {
        setImage(new GreenfootImage("You lost!", 30, Color.BLACK, null));
    }    
}
