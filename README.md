# Story of a Dream

Date of development: Apr 2014 - May 2014

2D Arcade game, presenting 6 short levels with different mechanics and gameplay.
Language: Java
IDE: Greenfoot

First level: run away from 3 balls
Second level: catch a ball that teleports randomly
Third level: escape mountain monsters and collect coins
Fourth level: destroy enemy ships
Fifth level: avoid birds while in endless fall
Sixth level: jump over boxes
